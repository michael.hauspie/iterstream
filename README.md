# `iterstream`

This crate provides a trait that can convert an `Iterator` to a
`Stream`. It uses the `futures` crate to create an executor that
gets the values from the iterator in a separate thread pool

It differs from the [`iter()`](futures::stream::iter) function
because iterator consumption is done in a separate thread. The
stream is then really asynchronous if the iterator is blocking

The `ThreadPool` needed to execute tasks can be either create
automatically (when using
[`to_stream`](crate::IterStream::to_stream)), or explicitly
specified using
[`to_stream_with_pool`](crate::IterStream::to_stream_with_pool). The
later is more flexible (and efficient) as it allows to share the
same pool for multiple streams

# Example
 ```rust
 use iterstream::IterStream;
 use futures::stream::StreamExt;
 use futures::executor::ThreadPool;

 let vals = vec![1, 2, 3, 4, 5];
 let stream = vals.into_iter().to_stream_with_pool(10, ThreadPool::new().unwrap());
 let c: Vec<_> = stream.collect().await;
 assert_eq!(vec![1,2,3,4, 5], c);
 ```

# Authors

- Michaël Hauspie

# Licence

- This crate is provided under [CeCILL-B](https://cecill.info/index.en.html) licence
